#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

#include <ctime>
#include <cstdlib>

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

int birdEye = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window, Camera& camera) {
    Matrix trans;

	//these are leftovers from lab6
    const float ROT = 1;
    const float SCALE = .05;
	
	//how much is translated at once.
    const float TRANS = .01;
	
    // TRANSLATE
    if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
	else if (isPressed(window, GLFW_KEY_T)) {
		if(birdEye == 0){
			camera.up = Vector(0, 0, 1);
			camera.eye = Vector(0, 3, 0);
			birdEye = 1;
		}
		else{
			camera.up = Vector(0, 1, 0);
			camera.eye = Vector(0, 0, 5);
			birdEye = 0;
		}
	}

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window, Camera& camera) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window, camera);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
    Model obj(
            Cone(20, 0.75, 0.2, 0.2).coords,
            Shader("../vert.glsl", "../frag.glsl"));
	Matrix obj_trans, obj_scale;
    obj_trans.translate(0, 0, -1);
    obj_scale.scale(0.25, 0.25, 0.25);
    obj.model = obj_trans*obj_scale;
			
    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, 0, -2);
    floor_scale.scale(100, 100, 1);
    floor.model = floor_trans*floor_scale;
	
	//old hardcoded single layer
	/*
	Model maze( //left
        Maze().coords,
        Shader("../vert.glsl", "../frag.glsl"));
    Matrix maze_trans, maze_scale;
    maze_trans.translate(-0.45, 0, -1);
    maze_scale.scale(0.1, 1, 1);
    maze.model = maze_trans*maze_scale;
	
	Model maze2( //bottom
        Maze().coords,
        Shader("../vert.glsl", "../frag.glsl"));
    Matrix maze_trans2, maze_scale2;
    maze_trans2.translate(0, -0.5, -1);
    maze_scale2.scale(1, 0.1, 1);
    maze2.model = maze_trans2*maze_scale2;

	Model maze3( //right
        Maze().coords,
        Shader("../vert.glsl", "../frag.glsl"));
    Matrix maze_trans3, maze_scale3;
    maze_trans3.translate(0.45, 0, -1);
    maze_scale3.scale(0.1, 1, 1);
    maze3.model = maze_trans3*maze_scale3;

	Model maze4( //top right
        Maze().coords,
        Shader("../vert.glsl", "../frag.glsl"));
    Matrix maze_trans4, maze_scale4;
    maze_trans4.translate(0.25, 0.5, -1);
    maze_scale4.scale(0.5, 0.1, 1);
    maze4.model = maze_trans4*maze_scale4;
	*/
	
	Model mazeParts[20] { //defines a whole bunch of basic cubes.
		//each block of 4 is a layer
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")},
		{Maze().coords, Shader("../vert.glsl", "../frag.glsl")}
	};
	
	int curLayer = 1; //layer multiplier
	srand(time(NULL));//seed the random
	
	//translates and scales the cubes into the maze.
	//I did it this way so I could sort of randomize the maze.
	for(int i = 0; i < 20; i+=4){
		//z scale always stays the same, so does z position. Don't need to do anything in those dimensions.
	
		//number used to determine which of the 4 sides to not draw
		int ranNum = rand() % 3;	

		//left
		Matrix maze_transL, maze_scaleL;
		maze_transL.translate(-0.47*curLayer*1.04, 0, -1);
		maze_scaleL.scale(0.1, 1*curLayer, 1);
		mazeParts[i].model = maze_transL*maze_scaleL; 

		//bottom
		Matrix maze_transB, maze_scaleB;
		maze_transB.translate(0, -0.5*curLayer, -1);
		maze_scaleB.scale(1*curLayer, 0.1, 1);
		mazeParts[i+1].model = maze_transB*maze_scaleB;
	
		//right
		Matrix maze_transR, maze_scaleR;
		maze_transR.translate(0.47*curLayer*1.03, 0, -1);
		maze_scaleR.scale(0.1, 1*curLayer, 1);
		mazeParts[i+2].model = maze_transR*maze_scaleR;

		//top
		Matrix maze_transT, maze_scaleT;
		maze_transT.translate(0, 0.5*curLayer, -1);
		maze_scaleT.scale(1*curLayer, 0.1, 1);
		mazeParts[i+3].model = maze_transT*maze_scaleT;

		/* //unused half block
		if(ranNum != 4){
			//top right
			Matrix maze_transTR, maze_scaleTR;
			maze_transTR.translate(0.25*layer, 0.5*layer, -1);
			maze_scaleTR.scale(0.5*layer, 0.1, 1);
			mazeParts[i+3].model = maze_transTR*maze_scaleTR;
		}
		*/
		
		//sets the scale of the undrawn size of the maze to 0, so you can't see it, kind of gross.
		Matrix temp, temp2;
		temp2.scale(0,0,0);
		mazeParts[i+ranNum].model = temp*temp2;
		
		//std::cout << rand() % 3;
		
		curLayer++;
	}
	
    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

	//starts in bird's eye by default.
    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 5);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(obj.model, window, camera);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
        // render the object, and floor
        renderer.render(camera, obj, lightPos);
        renderer.render(camera, floor, lightPos);
		
        //renderer.render(camera, maze, lightPos);
        //renderer.render(camera, maze2, lightPos);
        //renderer.render(camera, maze3, lightPos);
        //renderer.render(camera, maze4, lightPos);
		
		//the maze is a collection of models, so need to loop through.
		for(int i = 0; i < 20; i++){ 
			renderer.render(camera, mazeParts[i], lightPos);
		}
		
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
