Everything is mostly working except for regular camera view. 

Birds eye is in perspective, which seems to work well enough, though there are times where it's hard to see certain spots in the maze.

Shading works, but that was mostly done in lab6, which I based most of this off of. The light is just positioned way above the maze to the right, lighting some walls but not others.

The maze I tried to make slightly interesting by building a method where it could be randomized. 
To do this I built the maze out of several cubes, rather than having it be its own object, I looped through shapes to create it.
This made the maze pretty simple, but it wasn't too hard to throw in some randomization. It works by hiding one of the four walls in every layer.
It doesn't look amazing, but it was interesting at least. 
Since I just used cubes with preset normals, I didn't have to calculate them or do anything messy involving those.

The model you traverse the maze with is just a simple rotated cone, I wanted to make it rotate based on your last movement, so it would sort of point like an arrow, but ran out of time.

Use the arrow keys to move the cone, the T key was going to be the key to switch perspectives, but regular camera perspective isn't fully implemented.
