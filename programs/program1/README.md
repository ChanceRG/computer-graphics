# 2D Shape Modeller

Program is definitely not finished, but I will detail what was finished.

Stamp mode is sort of working, but you can only stamp a triangle, as the square is broken and I didn't finish the circle.

Modify mode is mostly complete, but has several bugs. Scaling is a bit wonky, and can have strange interactions if something has been
translated.

Translation mode translates, but it doesn't do it in a very good way.

Rotation mode works well, but it only rotates around the origin.

Coloring mode works pretty well, though it is somewhat of a weird method for doing it.

View mode was simple, so it works fine.

User defined mode was not implemented.
Grouping mode was not implemented.

As for how to operate the program.
Q swaps between edit and view mode.
When in edit mode, e swaps between Stamp, User Defined, Grouping, and Modify modes.
When in stamp mode, t selects triangle, selects square, c selects circle. To place a shape, click on the screen.
When in Modify mode, t selects translation, selects scale, c selects rotation. r selects red, g selects green, b selects blue.
When in scale mode, click and drag to scale up or down. 
When in rotate mode, click and hold to rotate.
When in translation mode, click and hold and the shape will slide to the cursor (kind of).
When in modify mode '=' increases the value of the selected color, while '-' decreases it.

I do feel like I learned a lot from this, sorry I couldn't finish the whole thing though, there was just a ton of stuff to do 
and I didn't have the time to do it. If I were to make a suggestion for the future, I would say to not make this due anywhere 
near the date of the midterm, or if you do, cut back on how much actual work is necessary for completion.