//Program 1
//Chance Ronning Glenn

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <vector>
#include <typeinfo>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

#define PI 3.14159265

int MODE = 0; //0 = view, 1 = edit
int EDITMODE = 1; // 1 = stamp, 2 = user defined primitive, 3 = group, 4 = modify.

bool pressing = false;
bool pressing2 = false;
bool mouseP = false;

int savedPos = -1;

int modType = 1; //1 = translate, 2 = scale, 3 = rotate.
int shapeType = 1; // 1 = triangle, 2 = square, 3 = circle.
int colorType = 1; // 1 = red, 2 = blue, 3 = green.
int rotation = 0; //just an angle for the rotation matrix

class Shape
{
	public:
	
	//shape data, by default a triangle.
	std::vector<float> vertices = {
		 0.5f,  0.5f, 0.5, 0.5, 0.5,
         0.5f, -0.5f, 0.5, 0.5, 0.5,
        -0.5f,  0.5f, 0.5, 0.5, 0.5,
	};
	
	//transformation matrices
	float transformation_matrix[9];
	
	float scale[9] = {1.01,0,0,0,1.01,0,0,0,1.01};
    float rotation[9] = {1,0,0,0,1,0,0,0,1}; 
    float translation[9] = {1,0,0,0,1,0,0,0,1};
	
	float * calcMatrix(){
		float translation_x_inverse = -1 * translation[6];
        float translation_y_inverse = -1 * translation[7];
		
        transformation_matrix[0] = scale[0]*rotation[0];
        transformation_matrix[1] = scale[0]*rotation[1];
        transformation_matrix[2] = 0;
        transformation_matrix[3] = scale[4]*rotation[3];
        transformation_matrix[4] = scale[4]*rotation[4];
        transformation_matrix[5] = 0;
        transformation_matrix[6] = translation[6]*scale[0]*rotation[0] + translation[7]*scale[4]*rotation[3] + translation_x_inverse;
        transformation_matrix[7] = translation[6]*scale[0]*rotation[1] + translation[7]*scale[4]*rotation[4] + translation_y_inverse;
        transformation_matrix[8] = 1;

		return transformation_matrix;
	}
};

std::vector<float> allVerts = {}; //vector that holds all shapes.

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void createRotationMatrix(float angle, float* buffer) {
    /* Method is used to create a rotation array at the specified input angle */
    buffer[0] = (float) cos(angle*PI/180);
    buffer[1] = (float) sin(angle*PI/180);
    buffer[3] = (float)-sin(angle*PI/180);
    buffer[4] = (float) cos(angle*PI/180);
}


void processInput(GLFWwindow *window, Shape &shape) {
	//close
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
	
	//view and edit mode swapped with q
	if (isPressed(window, GLFW_KEY_Q)) {
		pressing = true;
	}
	if(pressing && isReleased(window, GLFW_KEY_Q)){ //this is kind of ugly, but it works.
		if(MODE == 0){ //enter edit mode
			MODE = 1;
			std::cout << "Entering edit mode\n";
		}
		else{ //leave edit mode
			MODE = 0;
			std::cout << "Entering view mode\n";
		}
		pressing = false;
    }
	
	//if in edit mode.
	if(MODE == 1){
		if (isPressed(window, GLFW_KEY_E)) {
			pressing2 = true; 
		}
		if(pressing2 && isReleased(window, GLFW_KEY_E)){ //submode switching within edit mode.
			//switches mode with a button press.
			if(EDITMODE <= 4){
				EDITMODE++;
			}
			else{
				EDITMODE = 1;
			}
			
			if(EDITMODE == 1){ //stamp, default
				std::cout << "Entering Stamp Mode\n";
			}
			else if(EDITMODE == 2){ //user defined primitive
				std::cout << "Entering User Defined Primitive Mode\n";
			}
			else if(EDITMODE == 3){ //grouping
				std::cout << "Entering Grouping Mode\n";
			}
			else if(EDITMODE == 4){ //modify
				std::cout << "Entering Modify Mode\n";
			}
			
			pressing2 = false;
		}
		
		//stamp mode
		if(EDITMODE == 1){
			//shape selection, doesn't need weird toggle code probably.

			//switch to triangle
			if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
				std::cout << "Triangle";
				shapeType = 1;
				//set placement shape to triangle
			}
			
			//switch to square
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
				std::cout << "Square";
				shapeType = 2;
				//set placement shape to square
			}
			
			//switch to circle
			if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
				std::cout << "Circle";
				shapeType = 3;
				//set placement shape to circle
			}
			
			//handles when the user left clicks.
			if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
				mouseP = true; // I hate this, I'll try to find a better way if I have time.
			}
			if (mouseP && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) {
				double xpos, ypos; //gets mouse position
				glfwGetCursorPos(window, &xpos, &ypos);
			
				int windowX;
				int windowY;
				glfwGetWindowSize(window, &windowX, &windowY); //deals with window rescalings
			
				float newX = ((xpos / windowX) * 2) - 1; //coordinators based on window.
				float newY = (((ypos / windowY) * 2) - 1) * -1;
			
				if(shapeType == 1) { //triangle
					//shape is placed based on mouse position.
					std::vector<float> triangle = {
						 newX,  newY, 0.5f, 0.5, 0.5,
						 newX, newY + 0.5f, 0.5, 0.5, 0.5,
						 newX - 0.5f,  newY, 0.5, 0.5, 0.5,
					};
					
					shape.vertices.insert(std::end(shape.vertices), std::begin(triangle), std::end(triangle));
				}
				
				else if(shapeType == 2) { //square
					std::vector<float> square = {
						 newX, newY, 0.5f, 0.5, 0.5,
						 newX, newY + 0.5f, 0.5, 0.5, 0.5,
						 newX - 0.5f, newY, 0.5, 0.5, 0.5,
						 newX, newY + 0.5f, 0.5f, 0.5, 0.5,
						 newX - 0.5f, newY + 0.5f, 0.5, 0.5, 0.5,
						 newX - 0.5f, newY, 0.5, 0.5, 0.5,
					};

					shape.vertices.insert(std::end(shape.vertices), std::begin(square), std::end(square));
				}
				
				else { //circle
					//unfinished
					std::vector<float> circle = {
						 0.5f, -0.5f, 0.5, 0.5, 0.5,
						-0.5f, -0.5f, 0.5, 0.5, 0.5,
						-0.5f,  0.5f, 0.5, 0.5, 0.5,
					};

					int radius = 0.25;
					int h = ((xpos / windowX) * 2) - 1;
					int k = (((ypos / windowY) * 2) - 1) * -1;
					
					Shape temp;
					temp.vertices = circle;
				}
				
				mouseP = false;
			}
		}	

		//user defined primitive mode
		if(EDITMODE == 2){
			//ask user for some kind of input
		}

		//grouping mode
		if(EDITMODE == 3){
			//group multiple shapes together, let them stamp the grouping.
		}
		
		//modify mode
		if(EDITMODE == 4){
			//translate, scale, or rotate based on mouse movement.
			if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) { //translate mode
				std::cout << "Translate\n";
				modType = 1;
			}
			
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) { //scale mode
				std::cout << "Scale\n";
				modType = 2;
			}
			
			if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) { //rotate mode
				std::cout << "Rotate\n";
				modType = 3;
			}

			if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) { //modify red value
				std::cout << "Red\n";
				colorType = 1;
			}
				
			if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) { //modify green value
				std::cout << "Green\n";
				colorType = 2;
			}
				
			if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) { //modify blue value
				std::cout << "Blue\n";
				colorType = 3;
			}
			
			if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
				double xpos, ypos;
				glfwGetCursorPos(window, &xpos, &ypos);
				
				if(savedPos == -1){ //uses saved pos as an indicator of mouse movement.
					savedPos = xpos;
				}
				
				if(modType == 1){ //Translate
					//shape tries to move towards the cursor when clocked
					std::cout << "Translating\n";
					
					int windowX;
					int windowY;
					glfwGetWindowSize(window, &windowX, &windowY); //deals with window rescalings
					float posX = (shape.vertices.data()[0] * windowX)/2 + 1; 
					float posY = (shape.vertices.data()[1] * windowY)/2 + 1;

					if(xpos > posX){
						shape.translation[6] = shape.translation[6] + 0.2;
					}
					else{
						shape.translation[6] = shape.translation[6] - 0.2;
					}
					
					if(ypos > posY){
						shape.translation[7] = shape.translation[7] - 0.2;
					}
					else{
						shape.translation[7] = shape.translation[7] + 0.2;
					}
					
				}
				else if(modType == 2){ //Scale, kind of wonky
					std::cout << "Scaling\n";
					shape.scale[0] = shape.scale[0] + (savedPos - xpos) / 100;
					shape.scale[4] = shape.scale[4] + (savedPos - xpos) / 100;
				}
				else{ //Rotate
					createRotationMatrix(rotation, shape.rotation);
					rotation++;
				}
				
				savedPos = xpos; //update the pos
			}
			
			//color changing with right click
			if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) { //increases value of selected color
				if(colorType == 1){ //red	
					for(int i = 2; i < shape.vertices.size() + 5; i+= 5){ //this seems to work, but it's kind of crude
						shape.vertices.data()[i] += 0.01;
					}
				}
				else if(colorType == 2){ //green
					for(int i = 3; i < shape.vertices.size() + 5; i+= 5){ 
						shape.vertices.data()[i] += 0.01;
					}
				}
				else{ //blue
					for(int i = 4; i < shape.vertices.size() + 5; i+= 5){ 
						shape.vertices.data()[i] += 0.01;
					}
				}
			}
			else if(glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS){ //decreases value of selected color
				if(colorType == 1){ //red
					for(int i = 2; i < shape.vertices.size() + 5; i+= 5){
						shape.vertices.data()[i] += -0.01;
					}
				}
				else if(colorType == 2){ //green
					for(int i = 3; i < shape.vertices.size() + 5; i+= 5){ 
						shape.vertices.data()[i] += -0.01;
					}
				}
				else{ //blue
					for(int i = 4; i < shape.vertices.size() + 5; i+= 5){ 
						shape.vertices.data()[i] += -0.01;
					}
				}
			}
		}
	}
	else{ //View mode.
	
		//zoom in
		if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
			std::cout << "Zooming in.";
			
			//scale everything bigger
			shape.scale[0] = shape.scale[0] + 0.01;
			shape.scale[4] = shape.scale[4] + 0.01;
		}

		//zoom out
		if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
			std::cout << "Zooming out.";
			
			//scale everything smaller
			shape.scale[0] = shape.scale[0] - 0.01;
			shape.scale[4] = shape.scale[4] - 0.01;
		}
	}
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Program 1", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }
	
	Shape shape; //default object.
	
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shape.vertices)*10, shape.vertices.data(), GL_STATIC_DRAW);
	
    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);
	
	
    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
	
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shape);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

		glBufferData(GL_ARRAY_BUFFER, sizeof(shape.vertices)*10, shape.vertices.data(), GL_STATIC_DRAW);
		
        // use the shader
        shader.use();
		
		int loc  = glGetUniformLocation(shader.id(), "transformation_matrix");
        glUniformMatrix3fv(loc, 1, GL_FALSE, shape.calcMatrix());
		
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(shape.vertices.data()));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
		
		usleep(5000);
    }

    glfwTerminate();
    return 0;
}