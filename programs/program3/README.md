Triangle ray tracing appears to be fully functional. However, there are some weird triangles in the middle of the sphere that are not completely correct.

Phong shading is mostly implemented, ambient shading was simple, and diffuse shading seems to work. Specular shading does not use ray tracing because I ran out of time to do it. It works but it uses the old method.

My original plan for the +1 was to make soft shadows. However, I simply ran out of time, and could not complete the task.
My idea for how I would do this was I would scale the darkness of the shadow based on distance between the two intersections on the sphere, but I simply ran out of time and wasn't able to get there.

I made it so ray tracing is only performed once, that way you can sort of see what it does without any blind spots, it also doesn't use as much cpu this way.
U, I to rotate on X axis
O, P to rotate on Y axis
[, ] to rotate on Z axis
-, = to scale up and down
arrow keys to move the sphere around
, . to move sphere back and forward.

None of these will change the lighting, but they will allow you to see how the lighting is. There is a clear distortion around the center of the sphere, 
where only half the triangles are seemingly detected as facing the light source.

