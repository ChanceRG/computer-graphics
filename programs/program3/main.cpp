#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "Primitives.cpp"

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

#include <ctime>
#include <cstdlib>

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
const int DIST = 2;

std::vector<Ray> rays;

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

float determinant(std::vector<float> matr) {
	float first;
	float second;
	float third;
	float total;
	
	/*
		0 1 2
		3 4 5
		6 7 8

		((4 * 8) - (5 * 7)) * 0
		+
		((3 * 8) - (6 * 5)) * 1 * -1
		+
		((4 * 8) - (5 * 7)) * 2
	*/
	
	first = (matr[4] * matr[8] - matr[5] * matr[7]) * matr[0];
	second = (matr[3] * matr[8] - matr[6] * matr[5]) * matr[1] * -1;
	third = (matr[3] * matr[7] - matr[6] * matr[4]) * matr[2];
	
	total = first + second + third;
	
	return total;
}

float cramer(std::vector<float> original, std::vector<float> column) {
	float d, dx, dy, dz;
	
	float x, y, z;
	
	d = determinant(original);
	
	std::vector<float> temp = original;

	/*
		a 1 2
		b 4 5 
		c 7 8
	*/
	
	temp[0] = column[0];
	temp[3] = column[1];
	temp[6] = column[2];

	dx = determinant(temp);
	temp = original;
	
	/*
		0 a 2
		3 b 5 
		6 c 8
	*/
	
	temp[1] = column[0];
	temp[4] = column[1];
	temp[7] = column[2];

	dy = determinant(temp);
	temp = original;
	
	/*
		0 1 a
		3 4 b 
		6 7 c
	*/
	
	temp[2] = column[0];
	temp[5] = column[1];
	temp[8] = column[2];
	
	dz = determinant(temp);
	
	if(d == 0){
		
	}
	
	x = dx / d;
	y = dy / d;
	z = dz / d;
	
	return x, y, z;
} 

Matrix processModel(const Matrix& model, GLFWwindow *window, Camera& camera) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window, Camera& camera) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window, camera);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
    Model obj(
            Sphere(25, 1, 0.4, 0.8, 0.4).coords,
            Shader("../vert.glsl", "../frag.glsl"));

	
	//raytrace color modification
	bool intersect;
	for (int i = 0; i < obj.savedCoords.size() + 27; i+= 27){
		intersect = true; //whether or not the trace is intersecting with anything
	
		//just to make it a little cleaner looking
		std::vector<float> shape = obj.savedCoords;
		
		//the current triangle
		std::vector<float> tempTriangle = {
			shape[i+9] - shape[i], shape[i+18] - shape[i], 0,
			shape[i+10] - shape[i+1], shape[i+19] - shape[i+1], 0,
			shape[i+11] - shape[i+2], shape[i+20] - shape[i+2], -1
		};
		
		//the ray
		std::vector<float> tempRay = {0 - shape[i], 0 - shape[i+1], 0 - shape[i+2]}; 
		
		//uses cramer's rule to find v, u, and t
		float v,u,t = cramer(tempTriangle, tempRay);
			
		//checks if there's an intersection or not
		if(v < 0 || v > 1){ //no intersect
			intersect = false;
		}
		if(u <= 0 || (1 - v) < u){ //no intersect
			intersect = false;
		}
		if(t <= 0){ //no intersect
			intersect = false;
		}
		
		//if there is an intersection
		float colorAdd = 0.1;
		
		if(intersect){ //lighting, just adds to the colors equally.
			obj.savedCoords[i+3] += colorAdd;
			obj.savedCoords[i+4] += colorAdd;
			obj.savedCoords[i+5] += colorAdd;
			obj.savedCoords[i+12] += colorAdd;
			obj.savedCoords[i+13] += colorAdd;
			obj.savedCoords[i+14] += colorAdd;
			obj.savedCoords[i+21] += colorAdd;
			obj.savedCoords[i+22] += colorAdd;
			obj.savedCoords[i+23] += colorAdd;
		}
		
		//ray should be marked to indicate that it has collided with something so that a shadow could be cast behind it.
		//for soft shadows a reduction in light based on distance between intersection points. Short distance = light shadow.
		
		//loads the modified colors in
        glBufferData(GL_ARRAY_BUFFER, obj.size, obj.savedCoords.data(), GL_STATIC_DRAW);
	}
	
	
    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

	//perspective camera
    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 3);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position, this is from the older programs, not used in raytracing, still used for specular.
    Vector lightPos(2.75f, 2.75f, 3.0f);	
	
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
	
        // process input
        processInput(obj.model, window, camera);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
        // render the objects
        renderer.render(camera, obj, lightPos);
		
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

/*
[bx - ax | cx - ax | -dx]  [u]  =  [ex - ax]
[by - ay | cy - ay | -dy]  [v]  =  [ey - ay]
[bz - az | cx - az | -dz]  [t]  =  [ez - az]
*/

