/*
   Conner Becker, Samuel Congdon, Chance Ronning
   CSCI 441: Graphics
   Lab 2: Hardware Rasterization
   February 6, 2018

   This c++ program is utilizes rasterization to create a color gradient on a triangle. The three corner points 
   of the triangle have their coordinates and rgb colors defined by user input. The program utilizes OpenGL and
   GLSL to render the triangle using a GPU. 
   
   Example user input:
   50,50:1,0,0
   600,20:0,1,0
   300,400:0,0,1
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

// |{x,y,r,g,b}| * 3 = 15
#define TRIANGLE_BUFFER_SIZE 15
#define VERTICE_DESCRIPTORS 5
#define WINDOW_W 640
#define WINDOW_H 480
#define LOG_SIZE 512

/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

float normalizeX(float x) {
    return -1.0f + 2.0f*(x/(float)WINDOW_W);
}

float normalizeY(float y) {
    return 1.0f - 2.0f*(y/(float)WINDOW_H);
}

void promptPolygon(int numVertices, float* coordArray) {
    /* This function prompts the user for the coordinates and colors of each vertice in the polygon*/

    // prompt user for number of input coordinates and their rgb values
    std::cout << "Enter " << numVertices << " points (enter a point as x,y:r,g,b):" <<std::endl;

    char separator;
    // retrieve the information of each user entered point. 
    // separator is used to parse the floating point values out of commma/colon separated entries
    for (int i=0; i<numVertices; i++) {
        std::cin >> coordArray[i*VERTICE_DESCRIPTORS] >> separator 
                 >> coordArray[i*VERTICE_DESCRIPTORS+1] >> separator 
                 >> coordArray[i*VERTICE_DESCRIPTORS+2] >> separator 
                 >> coordArray[i*VERTICE_DESCRIPTORS+3] >> separator
                 >> coordArray[i*VERTICE_DESCRIPTORS+4];
                 
                 coordArray[i*VERTICE_DESCRIPTORS] = normalizeX(coordArray[i*VERTICE_DESCRIPTORS]); 
                 coordArray[i*VERTICE_DESCRIPTORS+1] = normalizeY(coordArray[i*VERTICE_DESCRIPTORS+1]);
                 //std::cout << "Normalized coordinates: " << coordArray[i*VERTICE_DESCRIPTORS] << ", " << coordArray[i*VERTICE_DESCRIPTORS+1] << std::endl;
    }
} // end promptPolygon

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(WINDOW_W, WINDOW_H, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }
    
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */
    GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[LOG_SIZE];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, LOG_SIZE, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program = 0;
    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[LOG_SIZE];
        glGetProgramInfoLog(program, LOG_SIZE, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

int main(void) {
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */
    float polygon[TRIANGLE_BUFFER_SIZE];
    promptPolygon(3, polygon);
    

    // convert the triangle to an array of floats containing
    // normalized device coordinates and color components.
    // float triangle[] = ...

    /** PART2: map the data */

    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
    GLuint VBO[1], VAO[1];
    glGenBuffers(1, VBO);
    glGenVertexArrays(1, VAO);

    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData
    glBindVertexArray(*VAO);
    glBindBuffer(GL_ARRAY_BUFFER,*VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(polygon), polygon, GL_STATIC_DRAW);
    // setup the attribute pointer for the coordinates
    // setup the attribute pointer for the colors
    // both will use glVertexAttribPointer and glEnableVertexAttribArray;
    glVertexAttribPointer(0,2,GL_FLOAT,GL_TRUE,VERTICE_DESCRIPTORS*sizeof(float),(void*)0);
    glEnableVertexAttribArray(0);
    
    glVertexAttribPointer(1,3,GL_FLOAT,GL_TRUE,VERTICE_DESCRIPTORS*sizeof(float),(void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    /** PART3: create the shader program */

    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

    // create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    /** END INITIALIZATION CODE */
    std::cout << "Entering Render Loop..." << std::endl;
    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
        glClearColor(.2f,.3f,.4f,1.0f);
        glClear(GL_COLOR_BUFFER_BIT);        

        // set the shader program using glUseProgram
        glUseProgram(shaderProgram);
        // bind the vertex array using glBindVertexArray
        glBindVertexArray(*VAO);
        // draw the triangles using glDrawArrays
        glDrawArrays(GL_TRIANGLES, 0, 3);

        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    std::cout << "Leaving Render Loop..." << std::endl;

    glfwTerminate();
    return 0;
}





