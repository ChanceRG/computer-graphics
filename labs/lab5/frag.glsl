#version 330 core
in vec3 ourColor;
in vec3 ourNorm;
in vec3 ourPosition;

uniform vec3 light;

uniform vec3 cameraPosition;

out vec4 fragColor;

void main() {

    float amb = 0.1;
    float shininess = 128;
    float specular_strength = 0.5;
    vec3 light_color = vec3(1,1,1);

    // calculate the diffusion value
    vec3 ourLight = light - ourPosition;
    float diff = dot(ourNorm, ourLight) / (length(ourLight) * length(ourNorm));
    diff = clamp(diff, 0, 1);
    vec3 diffuse = diff * light_color;

    // calculate the specular value
    vec3 light_surface = -ourLight;
    vec3 reflection = reflect(light_surface, ourNorm);
    vec3 surface_camera = normalize(cameraPosition - ourPosition);
    float angle = max(0, dot(surface_camera, reflection));
    float spec = pow(angle, shininess);
    vec3 specular = specular_strength * spec * light_color;
    
    // calculate the ambient light
    vec3 ambient = amb * light_color;

    vec3 modifier = ambient + diffuse + specular;

    fragColor = vec4(modifier * ourColor, 0.0f);
}
