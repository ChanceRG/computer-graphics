#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNorm;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;

uniform mat3 ITModel;
uniform vec3 light;

out vec3 ourColor;
out vec3 ourNorm;
out vec3 ourPosition;

void main() {
    ourPosition = vec3(model * vec4(aPos, 1.0f));
    ourNorm = vec3(normalize(ITModel * aNorm));

    gl_Position = projection * camera * model * vec4(aPos, 1.0);
    ourColor = aColor;
}
