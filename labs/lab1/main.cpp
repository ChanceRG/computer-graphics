/*
   Conner Becker, Samuel Congdon, Chance Ronning
   CSCI 441: Graphics
   Lab 1: Software Rasterization
   January 15, 2018

   This c++ program is utilizes rasterization to create a color gradient on a triangle. The three corner points 
   of the triangle have their coordinates and rgb colors defined by user input. The program then draws the trangle 
   and colors all the interior points based on their barycentric coordinates within the triangle. The figure is
   saved to triangle.bmp. Utilized point.hpp and bitmap_image.hpp. 

   Example user input:
   50,50:1,0,0
   600,20:0,1,0
   300,400:0,0,1

   References: 2000clicks.com/mathhelp/geometrytrianglebarycentriccoordinates.aspx
*/

#include <iostream>
#include "bitmap_image.hpp"
#include "point.hpp"

const int USERCOORDCOUNT = 3;

void getBoxBounds(float* xR, float* xL, float* yR, float* yL, Point* coords){
    /* Finds the box that is bounded the outermost coordinates of the triangle. */

	for(int i = 0; i < USERCOORDCOUNT; i++) {
			if(*xR < coords[i].x){
				*xR = coords[i].x;
			}
			if(*xL > coords[i].x){
				*xL = coords[i].x;
			}
			if(*yR > coords[i].y){
				*yR = coords[i].y;
			}
			if(*yL < coords[i].y){
				*yL = coords[i].y;
			}
		}
}

void colorTriangle(Point* coords, float xR,float xL,float yR, float yL, bitmap_image* image, rgb_t* color){
    /* Loops over every point within the box, testing whether each point is within the bounds of the triangle.
       If the point is within the trangle, its color is determined by its barycetnric coordinates, otherwise 
       it is colored black. */

	float x;
	float y;
	float t1;	// t1 = (xd+cf+ey-yc-de-fx)/(ad+cf+eb-bc-de-fa)
	float t2;	// t2 = (ay+xf+eb-bx-ye-fa)/(ad+cf+eb-bc-de-fa)
	float t3;	// t3 = (ad+cy+xb-bc-dx-ya)/(ad+cf+eb-bc-de-fa)
	float a = coords[0].x;
	float b = coords[0].y;
	float c = coords[1].x;
	float d = coords[1].y;
	float e = coords[2].x;
	float f = coords[2].y;

    //for every coordinate within the square boundaries of the triangle
	for(float y = yL; y >= yR; y--){			
		for(float x = xL; x <= xR; x++){

            // calulate the various values for barycentric coordinates
			t1 = (x*d+c*f+e*y-y*c-d*e-f*x)/(a*d+c*f+e*b-b*c-d*e-f*a);
			t2 = (a*y+x*f+e*b-b*x-y*e-f*a)/(a*d+c*f+e*b-b*c-d*e-f*a);
			t3 = (a*d+c*y+x*b-b*c-d*x-y*a)/(a*d+c*f+e*b-b*c-d*e-f*a);

            // check if the coordinate is within the triangle
			if(t1 > 0 && t2 > 0 && t3 > 0 && t1+t2+t3 <= 1.01){
                // if were in the triangle, calculate the color values based on the barycentric values and their relative point's colors 
				float red = 255 * (coords[0].r * t1 + coords[1].r * t2 + coords[2].r * t3);
				float green = 255 * (coords[0].g * t1 + coords[1].g * t2 + coords[2].g * t3);
				float blue = 255 * (coords[0].b * t1 + coords[1].b * t2 + coords[2].b * t3);
				rgb_t color2 = make_colour(red,green,blue);
				image->set_pixel(x,y,color2);
			}
		}										
	}											
}

void colorBox(float xR,float xL,float yR, float yL, bitmap_image* image, rgb_t* color){
	/* Colors the box bounded by the triangle coordinates to be white. Used in creating the program, not used in the final implementation. */ 

	for(float y = yL; y >= yR; y--){			// colors from left to right
		for(float x = xL; x <= xR; x++){		// top to bottom assuming 0,0
			image->set_pixel(x,y,*color);		// is in the lower left corner
												// since floating point coords
		}										// a single pixel may be lost
	}											// in the process of drawing
}

int main(int argc, char** argv) {

    Point coords[USERCOORDCOUNT];
    
    // prompt user for number of input coordinates and their rgb values
    std::cout << "Enter " << USERCOORDCOUNT << " points (enter a point as x,y:r,g,b):" <<std::endl;

    char separator;
    // retrieve the information of each user entered point. separator is used to parse the floating point values out of commma/colon separated entries
    for (int i=0;i<USERCOORDCOUNT;i++) {
        std::cin >> coords[i].x >> separator >> coords[i].y >> separator >> coords[i].r >> separator >> coords[i].g >> separator >> coords[i].b;
    }
    
    // output the entered coordinates
    std::cout << "You entered: " << std::endl;
    for (int i=0;i<USERCOORDCOUNT;i++) {
        std::cout << coords[i].x << ", " << coords[i].y << " : " << coords[i].r << ", " << coords[i].g << ", " << coords[i].b << std::endl;
    }
    
    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);
	
	float xR = coords[0].x;
	float xL = coords[0].x;
	float yR = coords[0].y;
	float yL = coords[0].y;
	
	getBoxBounds(&xR, &xL, &yR, &yL, coords);
	//std::cout <<"Box bounds: " << xL << "," << yL << " " << xR << "," << yR << std::endl;
	
	rgb_t color = make_colour(255, 255, 255);
	//colorBox(xR, xL, yR, yL, &image, &color);
	colorTriangle(coords, xR, xL, yR, yL, &image, &color);

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
