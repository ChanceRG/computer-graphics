/*
   Conner Becker, Samuel Congdon, Chance Ronning
   CSCI 441: Graphics
   Lab 3: Transformation Matrices
   February 13, 2018
    
   This c++ program demonstrates various transformation operations being performed on a square on a 2D plain, utilizing a single
   transformation matrix to perform the various transformations. Five modes are implemented, which can be iterated through by pressing 
   space with the program window open. Transformation operations implemented are scaling, rotation, and translation. The final mode
   combines all three of these operations to create a spiraling square. 

   GLFW is used to create the window, square, and acually move the square. This program creates the 3x3 transformation matrix, which
   is passed into the vert.glsl file. Once in the vert file, the current position of the square in (x, y) coordinates is padded into a
   3x1 matrix, which is then multiplied by the supplied 3x3 transformation matrix. The resulting 3x1 matrix then represents the updated
   position of the square, and is updated accordingly. 
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>
#include <unistd.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

#define PI 3.14159265

bool pressed = false;
int mode = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    // if the space bar is pressed, create a recording of the event
    else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        //std::cout << "SPACE" << std::endl;    
        pressed = true;
    }
    // if the space bar was pressed and has since been released, change the mode and remove the record of being pressed
    else if (pressed && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE){
        pressed = false;
        mode += 1;
        //std::cout << "released SPACE: " << mode << " : " << mode%5 << std::endl;    
    }
}

void createRotationMatrix(float angle, float* buffer) {
    /* Method is used to crete a rotation array at the specified input angle */
    buffer[0] = (float) cos(angle*PI/180);
    buffer[1] = (float) sin(angle*PI/180);
    buffer[3] = (float)-sin(angle*PI/180);
    buffer[4] = (float) cos(angle*PI/180);
    //std::cout << "angle" << angle << std::endl;
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }


    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
    
    float angle = 0;
    float scalar = 0.1;
    bool down = false;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */
        
        // the initial three submatrices that will be altered according to desired alterations
        // they will have no effect on the image unless they are altered
        float scale[9] = {1,0,0,0,1,0,0,0,1};
        float rotation[9] = {1,0,0,0,1,0,0,0,1}; 
        float translation[9] = {1,0,0,0,1,0,0,0,1};
        
        // mode for the static image (mode could be removed, it does nothing)
        if (mode%5 == 0) {
            scale[0] = 1;
            scale[4] = 1;
        }
        // mode to rotate around the origin
        else if (mode%5 == 1) {
            createRotationMatrix(angle,rotation);
            angle += 1;
        }
        // mode to rotate around a point that is not the origin
        else if (mode%5 == 2) {
            createRotationMatrix(angle,rotation);
            translation[6] = -0.5;
            translation[7] = -0.5;
            angle += 1;
        }
        // mode to scale the image up and down over time
        else if (mode%5 == 3) {
            if (scalar >= 1.5) {down = true;}
            else if (scalar <= 0.1) {down = false;}

            if (down) {scalar -= 0.01;}
            else {scalar += 0.01;}

            scale[0] = scalar;
            scale[4] = scalar;
        }
        // fun mode that were supposed to come up with
        else if (mode%5 == 4) {
            if (scalar >= .3) {down = true;}
            else if (scalar <= 0.1) {down = false;}

            if (down) {scalar -= 0.005;}
            else {scalar += 0.005;}
            scale[0] = scalar;
            scale[4] = scalar;
            createRotationMatrix(angle,rotation);
            translation[6] = cos(angle*PI/180)/2;
            translation[7] = sin(angle*PI/180)/2;
            angle+=1;
        }
        
        // calculate the inverse of the translation matrix to use later. the math can be simplified to these simple operations. 
        float translation_x_inverse = -1 * translation[6];
        float translation_y_inverse = -1 * translation[7];

        // create the final transformationmatix by combining all of the other matrices
        float transformation_matrix[9] = {
            scale[0]*rotation[0],
            scale[0]*rotation[1],
            0,
            scale[4]*rotation[3],
            scale[4]*rotation[4],
            0,
            translation[6]*scale[0]*rotation[0] + translation[7]*scale[4]*rotation[3] + translation_x_inverse,
            translation[6]*scale[0]*rotation[1] + translation[7]*scale[4]*rotation[4] + translation_y_inverse,
            1};

        // retrieve the location of our vert, then create and save the transformation matrix there
        int loc  = glGetUniformLocation(shader.id(), "transformation_matrix");
        glUniformMatrix3fv(loc, 1, GL_FALSE, transformation_matrix);
        
        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();

        usleep(5000);
    }

    glfwTerminate();
    return 0;
}



