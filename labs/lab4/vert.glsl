#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
uniform mat4 rotationX;
uniform mat4 rotationY;
uniform mat4 rotationZ;
uniform mat4 view;
uniform mat4 screen;
uniform mat4 proj;
uniform mat4 scale;
uniform mat4 trans;

out vec3 ourColor;
vec4 pos = vec4(aPos, 1.0);
mat4 rotation = rotationX * rotationY * rotationZ;
mat4 camera = proj*view*screen;

void main() {
    pos = pos * scale;
    pos = pos * rotation;
    pos = pos * trans;
    pos = camera*pos;
    gl_Position = vec4(pos);
    ourColor = aColor;
}
